/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import basedatos.personas.DAOImplCliente;
import basedatos.personas.DAOImplEmpleado;
import procesos.personas.Cliente;
import procesos.personas.Empleado;
import basedatos.BaseDatos;

/**
 *
 * @author Jhon
 */
public class CargaInformacion {
    public static void main(String[] args) {
        GeneradorObjetos go = new GeneradorObjetos();
        BaseDatos proceso;
        int guardado = 1, cantidadCliente = 100, cantidadEmpleado = 10;
        switch (guardado) {
            //Caso clientes
            case 0:
                proceso = new DAOImplCliente();
                for (int i = 0; i <= cantidadCliente; i++) {
                    Cliente cliente = new Cliente();
                    cliente.setIdentificaion(go.generadorIdentificaciones());
                    cliente.setNombres(go.generadorNombres());
                    cliente.setApellidos(go.generadorApellidos());
                    cliente.setTelefono("3"+ go.generadorIdentificaciones());
                    cliente.setEliminado(false);
                    String correo = cliente.getNombres().replace(" ", "").toLowerCase() + (int)(Math.random()*(99 - 10)) + "@correo.com";
                    cliente.setCorreo(correo);
                    cliente.setObservaciones("Informacion ramdon");
                    try {
                        proceso.registrar(cliente);
                        System.out.println("Cargado: " + i);
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
                break;
            //Caso empleados    
            case 1:
                proceso = new DAOImplEmpleado();
                for (int i = 0; i <= cantidadEmpleado; i++) {
                    Empleado empleados = new Empleado();
                    empleados.setIdentificaion(go.generadorIdentificaciones());
                    empleados.setNombres(go.generadorNombres().trim());
                    empleados.setApellidos(go.generadorApellidos().trim());
                    empleados.setTelefono("3"+ go.generadorIdentificaciones());
                    empleados.setEliminado(false);
                    String usuario = empleados.getNombres().substring(1, 4).toLowerCase() + (int)(Math.random()*(99 - 10));
                    empleados.setUsuario(usuario);
                    empleados.setContrasena("123456789");
                    try {
                        proceso.registrar(empleados);
                        System.out.println("Cargado: " + i);
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
                break;
                
            case 2:
                break;
        }
        
        
        
    }
}
