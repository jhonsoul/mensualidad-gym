/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utilidades;

import basedatos.personas.DAOImplEmpleado;
import java.util.logging.Level;
import java.util.logging.Logger;
import procesos.personas.Empleado;

/**
 *
 * @author Jhon Alexander
 */
public class CargaAdmin {
    public static void main(String[] args) {
        Empleado empleados = new Empleado();
        empleados.setIdentificaion("000000");
        empleados.setNombres("Admin");
        empleados.setApellidos("Gym");
        empleados.setTelefono("");
        empleados.setEliminado(false);
        empleados.setUsuario("admin");
        empleados.setContrasena("123456789");
        try {
            new DAOImplEmpleado().registrar(empleados);
            System.out.println("admin creado con contraseña: " + empleados.getContrasena());
        } catch (Exception ex) {
            Logger.getLogger(CargaAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
