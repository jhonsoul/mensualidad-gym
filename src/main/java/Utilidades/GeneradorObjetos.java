/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

/**
 *
 * @author Jhon
 */
public class GeneradorObjetos {
    
    public String generadorIdentificaciones() {
        String identificacion = (int)(Math.random()* 1000000000)+"";
        return identificacion;
    }
    
    public String generadorNombres() {
        String nombres;
        String[] nombreA ={"Hugo", "Isabella", "Martín", "Sophia", "Antonio", "Carlos", "Elizabeth"};
        String[] nombreB ={"Marcos", "", "Izan", "Adam", ""};
        nombres = nombreA[(int)(Math.random()*(nombreA.length - 0))] + " " + nombreB[(int)(Math.random()*(nombreB.length - 0))];
        return nombres;
    }
    
    public String generadorApellidos() {
        String apellidos;
        String[] apellidoA ={"Castillo", "Castro", "Contreras", "Díaz", "Duarte", "Espinoza", "Fernández", "Flores", "García", "Gutiérrez", "López", "Ramírez"};
        apellidos = apellidoA[(int)(Math.random()*(apellidoA.length - 0))] + " " + apellidoA[(int)(Math.random()*(apellidoA.length - 0))];
        return apellidos;
    }
}
