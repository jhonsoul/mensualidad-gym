/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import procesos.general.EncriptadorDesEncriptador;

/**
 *
 * @author Jhon
 */
public class Pruebas {
    public static void main(String[] args) {
        final String claveEncriptacion = "obsH47";
        String datosOriginales = "Mensajes ocultos";
        EncriptadorDesEncriptador encriptador = new EncriptadorDesEncriptador();
        String encriptado = encriptador.encriptar(datosOriginales);
        String desencriptado = encriptador.desencriptar(encriptado);
        System.out.println("Cadena Original: " + datosOriginales);
        System.out.println("Escriptado     : " + encriptado);
        System.out.println("Desencriptado  : " + desencriptado);
         
    }
}
