/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.cliente;

import basedatos.BaseDatos;
import basedatos.personas.DAOImplCliente;
import java.util.Arrays;
import java.util.List;
import javax.swing.JOptionPane;
import procesos.general.LimitaCamposTexto;
import procesos.personas.Cliente;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public class EliminarClienteVentana extends javax.swing.JPanel {

    LimitaCamposTexto validaciones;
    
    /**
     * Creates new form EliminarClienteVentana
     */
    public EliminarClienteVentana() {
        initComponents();
        validaciones = new LimitaCamposTexto();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel17 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        identificarEliminarCliente = new javax.swing.JTextField();
        buscarEliminarClienteBoton = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        nombresTexto = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        apellidosTexto = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        telefonoTexto = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        eliminarClienteBoton = new javax.swing.JButton();
        eliminarClienteBotonLimpiar = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        correoTexto = new javax.swing.JLabel();
        observacionesTexto = new javax.swing.JLabel();

        jPanel17.setBackground(new java.awt.Color(72, 0, 0));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Identificación:");

        identificarEliminarCliente.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        identificarEliminarCliente.setPreferredSize(new java.awt.Dimension(300, 40));
        identificarEliminarCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                identificarEliminarClienteKeyTyped(evt);
            }
        });

        buscarEliminarClienteBoton.setText("Buscar");
        buscarEliminarClienteBoton.setOpaque(false);
        buscarEliminarClienteBoton.setPreferredSize(new java.awt.Dimension(120, 40));
        buscarEliminarClienteBoton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarEliminarClienteBotonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(identificarEliminarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 476, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buscarEliminarClienteBoton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(identificarEliminarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buscarEliminarClienteBoton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26))
        );

        jPanel13.setBackground(new java.awt.Color(51, 51, 51));
        jPanel13.setPreferredSize(new java.awt.Dimension(382, 370));

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setOpaque(false);
        jPanel14.setLayout(new java.awt.GridLayout(6, 0, 0, 10));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(204, 204, 204));
        jLabel12.setText("Nombres");
        jLabel12.setPreferredSize(new java.awt.Dimension(63, 40));
        jPanel14.add(jLabel12);

        nombresTexto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nombresTexto.setForeground(new java.awt.Color(153, 204, 255));
        nombresTexto.setPreferredSize(new java.awt.Dimension(34, 40));
        jPanel14.add(nombresTexto);

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(204, 204, 204));
        jLabel13.setText("Apellidos");
        jPanel14.add(jLabel13);

        apellidosTexto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        apellidosTexto.setForeground(new java.awt.Color(153, 204, 255));
        jPanel14.add(apellidosTexto);

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(204, 204, 204));
        jLabel28.setText("Teléfono");
        jPanel14.add(jLabel28);

        telefonoTexto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        telefonoTexto.setForeground(new java.awt.Color(153, 204, 255));
        jPanel14.add(telefonoTexto);

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(50, Short.MAX_VALUE)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );

        jPanel18.setBackground(new java.awt.Color(72, 0, 0));

        eliminarClienteBoton.setText("Eliminar");
        eliminarClienteBoton.setOpaque(false);
        eliminarClienteBoton.setPreferredSize(new java.awt.Dimension(90, 40));
        eliminarClienteBoton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarClienteBotonActionPerformed(evt);
            }
        });

        eliminarClienteBotonLimpiar.setText("Limpiar");
        eliminarClienteBotonLimpiar.setOpaque(false);
        eliminarClienteBotonLimpiar.setPreferredSize(new java.awt.Dimension(90, 40));
        eliminarClienteBotonLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarClienteBotonLimpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(eliminarClienteBoton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(61, 61, 61)
                .addComponent(eliminarClienteBotonLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(413, 413, 413))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(eliminarClienteBoton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(eliminarClienteBotonLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel15.setBackground(new java.awt.Color(51, 51, 51));
        jPanel15.setPreferredSize(new java.awt.Dimension(382, 370));

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setOpaque(false);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(204, 204, 204));
        jLabel14.setText("Correo");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(204, 204, 204));
        jLabel15.setText("Observaciones");

        correoTexto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        correoTexto.setForeground(new java.awt.Color(153, 204, 255));
        correoTexto.setPreferredSize(new java.awt.Dimension(34, 40));

        observacionesTexto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        observacionesTexto.setForeground(new java.awt.Color(153, 204, 255));

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, 314, Short.MAX_VALUE)
            .addComponent(correoTexto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(observacionesTexto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(correoTexto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(observacionesTexto, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 194, Short.MAX_VALUE)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55))
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void identificarEliminarClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_identificarEliminarClienteKeyTyped
        validaciones.limitadorCamposNumericos(evt, identificarEliminarCliente);
    }//GEN-LAST:event_identificarEliminarClienteKeyTyped

    private void buscarEliminarClienteBotonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarEliminarClienteBotonActionPerformed
        if (!identificarEliminarCliente.getText().isEmpty()) {
            BaseDatos proceso = new DAOImplCliente();
            try {
                Cliente cliente = (Cliente) proceso.buscar(identificarEliminarCliente.getText());
                if (cliente != null) {
                    nombresTexto.setText(cliente.getNombres());
                    apellidosTexto.setText(cliente.getApellidos());
                    telefonoTexto.setText(cliente.getTelefono());
                    correoTexto.setText(cliente.getCorreo());
                    observacionesTexto.setText(cliente.getObservaciones());
                    activaBusqueda(false);
                } else {
                    JOptionPane.showConfirmDialog(null, "El cliente inexistente", "Error por falta de información", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                JOptionPane.showConfirmDialog(null, "Error al acceder a la base de datos", "Error en conexión a la base de datos", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showConfirmDialog(null, "Para buscar el cliente se debe ingresar la identificación", "Error por falta de información", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_buscarEliminarClienteBotonActionPerformed

    private void eliminarClienteBotonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarClienteBotonActionPerformed
        if (!identificarEliminarCliente.getText().isEmpty()) {
            String id = identificarEliminarCliente.getText();
            Cliente cliente = new Cliente();
            cliente.setIdentificaion(id);
            BaseDatos proceso = new DAOImplCliente();
            try {
                proceso.eliminar(cliente);
                limpiarCampos();
            } catch (Exception ex) {
                System.out.println("Error al intentar eliminar el empleado");
            }
        }
    }//GEN-LAST:event_eliminarClienteBotonActionPerformed

    private void eliminarClienteBotonLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarClienteBotonLimpiarActionPerformed
        limpiarCampos();
        activaBusqueda(true);
    }//GEN-LAST:event_eliminarClienteBotonLimpiarActionPerformed

    private void activaBusqueda(boolean activo) {
        identificarEliminarCliente.setEnabled(activo);
        buscarEliminarClienteBoton.setEnabled(activo);
    }
    
     private void limpiarCampos() {
        validaciones = new LimitaCamposTexto();
        List<javax.swing.JLabel> camposTexto = Arrays.asList(observacionesTexto, nombresTexto, apellidosTexto, telefonoTexto, correoTexto);
        camposTexto.forEach(campo -> campo.setText(""));
        identificarEliminarCliente.setText("");
        activaBusqueda(true);
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel apellidosTexto;
    private javax.swing.JButton buscarEliminarClienteBoton;
    private javax.swing.JLabel correoTexto;
    private javax.swing.JButton eliminarClienteBoton;
    private javax.swing.JButton eliminarClienteBotonLimpiar;
    private javax.swing.JTextField identificarEliminarCliente;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JLabel nombresTexto;
    private javax.swing.JLabel observacionesTexto;
    private javax.swing.JLabel telefonoTexto;
    // End of variables declaration//GEN-END:variables
}
