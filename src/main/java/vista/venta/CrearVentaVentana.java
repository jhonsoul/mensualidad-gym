/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.venta;

import basedatos.BaseDatos;
import basedatos.Tarifas.DAOImplTarifa;
import basedatos.personas.DAOImplCliente;
import basedatos.venta.DAOImplVenta;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import procesos.Tarifas.Tarifa;
import procesos.general.LimitaCamposTexto;
import procesos.personas.Cliente;
import procesos.personas.Empleado;
import procesos.ventas.Venta;
import vista.tarifa.EditarTarifaVentana;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public class CrearVentaVentana extends javax.swing.JPanel {

    private Cliente cliente;
    private Tarifa tarifa;
    private Empleado empleado;
    private LocalDate fechaTermina;
    
    private final LimitaCamposTexto validaciones;
    
    /**
     * Creates new form CrearVentaVentana
     */
    public CrearVentaVentana() {
        initComponents();
        cargarComboBox();
        fechaPago.setText(LocalDate.now().toString());
        validaciones = new LimitaCamposTexto();
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel6 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        identificacionClienteVenta = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        nombresCliente = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        nombreTarifaCB = new javax.swing.JComboBox<>();
        jLabel29 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel30 = new javax.swing.JLabel();
        fechaPago = new javax.swing.JFormattedTextField();
        jPanel19 = new javax.swing.JPanel();
        btnoCrearVenta = new javax.swing.JButton();
        limpiarEmpleado = new javax.swing.JButton();

        jPanel6.setBackground(new java.awt.Color(51, 51, 51));
        jPanel6.setPreferredSize(new java.awt.Dimension(382, 370));

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setOpaque(false);
        jPanel8.setLayout(new java.awt.GridLayout(6, 1, 0, 10));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(204, 204, 204));
        jLabel8.setText("Identificación");
        jPanel8.add(jLabel8);

        identificacionClienteVenta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        identificacionClienteVenta.setToolTipText("");
        identificacionClienteVenta.setPreferredSize(new java.awt.Dimension(300, 40));
        identificacionClienteVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                identificacionClienteVentaKeyTyped(evt);
            }
        });
        jPanel8.add(identificacionClienteVenta);

        jButton1.setText("Buscar Cliente");
        jButton1.setOpaque(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel8.add(jButton1);
        jPanel8.add(jLabel1);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(204, 204, 204));
        jLabel10.setText("Cliente");
        jPanel8.add(jLabel10);

        nombresCliente.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nombresCliente.setForeground(new java.awt.Color(153, 204, 255));
        jPanel8.add(nombresCliente);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel9.setBackground(new java.awt.Color(51, 51, 51));
        jPanel9.setPreferredSize(new java.awt.Dimension(382, 370));

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setOpaque(false);
        jPanel10.setLayout(new java.awt.GridLayout(7, 1, 0, 10));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(204, 204, 204));
        jLabel11.setText("ID Tarifa");
        jPanel10.add(jLabel11);

        nombreTarifaCB.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nombreTarifaCB.setOpaque(false);
        nombreTarifaCB.setPreferredSize(new java.awt.Dimension(114, 40));
        nombreTarifaCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreTarifaCBActionPerformed(evt);
            }
        });
        jPanel10.add(nombreTarifaCB);

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(204, 204, 204));
        jLabel29.setText("Valor a pagar");
        jPanel10.add(jLabel29);

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(153, 204, 255));
        jPanel10.add(jLabel2);
        jPanel10.add(jSeparator1);

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(204, 204, 204));
        jLabel30.setText("Fecha Inicio (Año-Mes-Dia)");
        jPanel10.add(jLabel30);

        fechaPago.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("y-MM-d"))));
        jPanel10.add(fechaPago);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );

        jPanel19.setBackground(new java.awt.Color(72, 0, 0));

        btnoCrearVenta.setText("Crear Venta");
        btnoCrearVenta.setOpaque(false);
        btnoCrearVenta.setPreferredSize(new java.awt.Dimension(90, 40));
        btnoCrearVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnoCrearVentaActionPerformed(evt);
            }
        });

        limpiarEmpleado.setText("Limpiar");
        limpiarEmpleado.setOpaque(false);
        limpiarEmpleado.setPreferredSize(new java.awt.Dimension(90, 40));
        limpiarEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limpiarEmpleadoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel19Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnoCrearVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(61, 61, 61)
                .addComponent(limpiarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(413, 413, 413))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel19Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnoCrearVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(limpiarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 149, Short.MAX_VALUE)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(80, 80, 80))
            .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 72, Short.MAX_VALUE)
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (!identificacionClienteVenta.getText().isEmpty()) {
            BaseDatos proceso = new DAOImplCliente();
            try {
                cliente = (Cliente) proceso.buscar(identificacionClienteVenta.getText());
                if (cliente != null) {
                    String nombres = cliente.getNombres() + " ";
                    nombres += cliente.getApellidos();
                    nombresCliente.setText(nombres);
                    limpiarCliente(false);
                } else {
                    JOptionPane.showConfirmDialog(null, "El cliente inexistente", "Error por falta de información", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                JOptionPane.showConfirmDialog(null, "Error al acceder a la base de datos", "Error en conexión a la base de datos", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showConfirmDialog(null, "Para buscar el cliente se debe ingresar la identificación", "Error por falta de información", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void nombreTarifaCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombreTarifaCBActionPerformed
        if (nombreTarifaCB.getSelectedIndex() > -1) cargarValor(nombreTarifaCB.getSelectedItem().toString());
    }//GEN-LAST:event_nombreTarifaCBActionPerformed

    private void limpiarEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limpiarEmpleadoActionPerformed
        limpiarCliente(true);
    }//GEN-LAST:event_limpiarEmpleadoActionPerformed

    private void identificacionClienteVentaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_identificacionClienteVentaKeyTyped
        validaciones.limitadorCamposNumericos(evt, identificacionClienteVenta);
    }//GEN-LAST:event_identificacionClienteVentaKeyTyped

    private void btnoCrearVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnoCrearVentaActionPerformed
        if (!identificacionClienteVenta.getText().isEmpty() && nombreTarifaCB.getSelectedIndex() > -1 && !fechaPago.getText().isEmpty()) {
            try {
                fechaTermina = tiempoReservado(nombreTarifaCB.getSelectedItem().toString());
                Venta venta = new Venta();
                venta.setIdentificacionCliente(identificacionClienteVenta.getText());
                venta.setIdentificacionTarifa(nombreTarifaCB.getSelectedItem().toString());
                venta.setIdentificacionEmpleado(empleado.getIdentificaion());
                venta.setFechaTermino(fechaTermina);
                venta.setEliminado(false);
                BaseDatos proceso = new DAOImplVenta();
                proceso.registrar(venta);
            } catch (Exception ex) {
                Logger.getLogger(CrearVentaVentana.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnoCrearVentaActionPerformed

    private void limpiarCliente(boolean activar) {
        identificacionClienteVenta.setEnabled(activar);
        jButton1.setEnabled(activar);
        if (activar) {
            identificacionClienteVenta.setText("");
            nombresCliente.setText("");
        }
    }
    
    private LocalDate tiempoReservado(String tiempo) {
        LocalDate fecha = LocalDate.parse(fechaPago.getText());
        if (tiempo.toLowerCase().contains("Quincen")) {
            fecha.plusDays(15);
        } else if (tiempo.toLowerCase().contains("Mensualidad")) {
            fecha.plusMonths(1);
        } else if (tiempo.toLowerCase().contains("Tremestre")) {
            fecha.plusMonths(3);
        } else if (tiempo.toLowerCase().contains("Semestre")) {
            fecha.plusMonths(6);
        }
        return fecha;
    }
    
    private void cargarComboBox() {
        try {
            nombreTarifaCB.removeAllItems();
            List<String> lista = new DAOImplTarifa().cargaIdTarifas();
            lista.forEach(x -> nombreTarifaCB.addItem(x));
        } catch (Exception ex) {
            Logger.getLogger(EditarTarifaVentana.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void cargarValor(String id) {
        System.out.println(id);
        if (id != null) {
            BaseDatos proceso = new DAOImplTarifa();
            try {
                tarifa = (Tarifa) proceso.buscar(id);
                if (tarifa != null) {
                    int valorTotal = tarifa.getPrecio();
                    if (tarifa.getImpuesto() > 0) valorTotal += valorTotal * tarifa.getImpuesto() / 100;
                    jLabel2.setText(String.valueOf(valorTotal));
                } else {
                    JOptionPane.showConfirmDialog(null, "La tarifa inexistente.", "Error por falta de información", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                JOptionPane.showConfirmDialog(null, "Error al acceder a la base de datos.", "Error en conexión a la base de datos", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showConfirmDialog(null, "Fallo al buscar la tarifa.", "Error por falta de información", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnoCrearVenta;
    private javax.swing.JFormattedTextField fechaPago;
    private javax.swing.JTextField identificacionClienteVenta;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JButton limpiarEmpleado;
    private javax.swing.JComboBox<String> nombreTarifaCB;
    private javax.swing.JLabel nombresCliente;
    // End of variables declaration//GEN-END:variables
}
