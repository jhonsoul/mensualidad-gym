/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos.personas;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Jhon
 */
@Data
@NoArgsConstructor
public class Empleado extends Personas{
    private String usuario;
    private String contrasena;

    public Empleado(String identificaion, String nombres, String apellidos, String telefono, boolean eliminado, String usuario, String contrasena) {
        super(identificaion, nombres, apellidos, telefono, eliminado);
        this.usuario = usuario;
        this.contrasena = contrasena;
    }
    
    
}
