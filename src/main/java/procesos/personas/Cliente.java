/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos.personas;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Jhon
 */
@Data
@NoArgsConstructor
public class Cliente extends Personas{
    private String correo;
    private String observaciones;
    
    
    public Cliente(String identificaion, String nombres, String apellidos, String telefono, boolean eliminado, String correo, String observaciones) {
        super(identificaion, nombres, apellidos, telefono, eliminado);
        this.correo = correo;
        this.observaciones = observaciones;
    }
    
}
