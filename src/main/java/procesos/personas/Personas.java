/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos.personas;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 *
 * @author Jhon
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Personas {
    private String identificaion;
    private String nombres;
    private String apellidos;
    private String telefono;
    private boolean eliminado;
}
