/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos.ventas;

import java.time.LocalDate;
import lombok.Data;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
@Data
public class Venta {
    private String identificacionCliente;
    private String identificacionEmpleado;
    private String identificacionTarifa;
    private LocalDate FechaTermino;
    private boolean eliminado;
}
