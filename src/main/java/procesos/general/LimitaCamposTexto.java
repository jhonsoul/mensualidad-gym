/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos.general;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public class LimitaCamposTexto {
    private final int TEXTOS_CORTOS = 10;
    private final int TEXTOS_LARGOS = 30;
    
    public void limitadorCampos(java.awt.event.KeyEvent e, javax.swing.JTextField cajaTexto) {
        if (cajaTexto.getText().length() >= TEXTOS_LARGOS) e.consume();
    }
    
    public void limitadorCamposNumericos(java.awt.event.KeyEvent e, javax.swing.JTextField cajaTexto) {
        if (cajaTexto.getText().length() >= TEXTOS_CORTOS || !Character.isDigit(e.getKeyChar())) e.consume();
    }
}
