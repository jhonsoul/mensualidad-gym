/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos.general;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Jhon
 */
public class EncriptadorDesEncriptador {
    
    private final String LLAVE = "una llave";
    
    private SecretKeySpec crearClave(String clave) {
        byte[] claveEncriptacion;
        try {
            claveEncriptacion = clave.getBytes("UTF-8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            claveEncriptacion = sha.digest(claveEncriptacion);
            claveEncriptacion = Arrays.copyOf(claveEncriptacion, 16);
            SecretKeySpec secretKey = new SecretKeySpec(claveEncriptacion, "AES");
            return secretKey;
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            Logger.getLogger(EncriptadorDesEncriptador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String encriptar (String texto) {
        try {
            SecretKeySpec secretKey = this.crearClave(LLAVE);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] datosEncriptar = texto.getBytes("UTF-8");
            byte[] bytesEncriptados = cipher.doFinal(datosEncriptar);
            String encriptado = Base64.getEncoder().encodeToString(bytesEncriptados);
            return encriptado;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException ex) {
            Logger.getLogger(EncriptadorDesEncriptador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String desencriptar(String textoEncriptado) {
        try {
            SecretKeySpec secretKey = this.crearClave(LLAVE);
            
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            
            byte[] bytesEncriptados = Base64.getDecoder().decode(textoEncriptado);
            byte[] datosDesencriptados = cipher.doFinal(bytesEncriptados);
            String datos = new String(datosDesencriptados);
            
            return datos;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(EncriptadorDesEncriptador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
