/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos.general;



/**
 *
 * @author Jhon
 */
public class ValidacionExpresionesRegulares {
    private final String PATRON_NOMBRE = "^[a-zA-ZÀ-ÿñÑ]+\\s*[a-zA-ZÀ-ÿñÑ]*\\s*$";
    private final String PATRON_USUARIO = "^[a-zA-Z0-9_-]{5,10}$";
    private final String PATRON_CONTRASENA = "^[a-zA-Z0-9_.-]{8,30}$";
    private final String PATRON_CORREO = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9_.-]+$";
    private final String PATRON_NUMERO = "^[0-9]{10}$";
    private final String PATRON_PRECIOS = "^[0-9]{1,10}$";
    
    public boolean validaNumeros(String dato) {
        return dato.matches(PATRON_NUMERO);
    }
    
    public boolean validaPrecios(String dato) {
        return dato.matches(PATRON_PRECIOS);
    }
    
    public boolean validaNombres(String dato) {
        return dato.matches(PATRON_NOMBRE) && dato.length() <= 30;
    }
    
    public boolean validaUsuario(String dato) {
        return dato.matches(PATRON_USUARIO);
    }
    
    public boolean validaContrasena(String dato) {
        return dato.matches(PATRON_CONTRASENA);
    }
    
    public boolean validaCorreo(String dato) {
        return dato.matches(PATRON_CORREO) && dato.length() <= 30;
    }
    
}
