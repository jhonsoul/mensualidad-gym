/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos.general;

import basedatos.BaseDatos;
import basedatos.Tarifas.DAOImplTarifa;
import basedatos.personas.DAOImplCliente;
import basedatos.personas.DAOImplEmpleado;
import basedatos.venta.DAOImplVenta;
import java.awt.Font;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public class ManejoTablas {
    
    private final BaseDatos proceso;
    private final JTable tablaActual;
    private final JButton botonMas;
    private final JButton botonMenos;
    private final JLabel textoPaginando;
    private int pagina;
    private final int LIMITE = 20;
    private int nRegistos;

    public ManejoTablas(BaseDatos proceso, JTable tablaActual, JButton botonMenos, JButton botonMas, JLabel textoPaginando) {
        this.proceso = proceso;
        this.tablaActual = tablaActual;
        this.botonMas = botonMas;
        this.botonMenos = botonMenos;
        this.textoPaginando = textoPaginando;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }
    
    public void cargaTabla() {
        try {
            nRegistos = proceso.cantidadRegistros();
            if (proceso instanceof DAOImplCliente) textoPaginando.setText(((pagina + 1) * LIMITE) - LIMITE + " de " + LIMITE * (1 + pagina) + " - " + nRegistos);
            else if (proceso instanceof DAOImplEmpleado) textoPaginando.setText(((pagina + 1) * LIMITE) - LIMITE + " de " + LIMITE * (1 + pagina) + " - " + nRegistos);
            else if (proceso instanceof DAOImplTarifa) textoPaginando.setText(((pagina + 1) * LIMITE) - LIMITE + " de " + LIMITE * (1 + pagina) + " - " + nRegistos);
            else if (proceso instanceof DAOImplVenta) textoPaginando.setText(((pagina + 1) * LIMITE) - LIMITE + " de " + LIMITE * (1 + pagina) + " - " + nRegistos);
            tablaActual.setModel(proceso.lista(pagina, LIMITE));
            javax.swing.table.JTableHeader encabezado = tablaActual.getTableHeader();
            encabezado.setFont(new Font("Tahoma", Font.BOLD, 15));
            botonesPaginado();
        } catch (Exception ex) {
            Logger.getLogger(ManejoTablas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cargaTabla(String id) {
        try {
            if (proceso instanceof DAOImplVenta) {
                nRegistos = new DAOImplVenta().cantidadRegistrosIndividual(id);
                textoPaginando.setText(((pagina + 1) * LIMITE) - LIMITE + " de " + LIMITE * (1 + pagina) + " - " + nRegistos);
                tablaActual.setModel(new DAOImplVenta().consultaIndividual(id, pagina, LIMITE));
            }
            javax.swing.table.JTableHeader encabezado = tablaActual.getTableHeader();
            encabezado.setFont(new Font("Tahoma", Font.BOLD, 15));
            botonesPaginado();
        } catch (Exception ex) {
            Logger.getLogger(ManejoTablas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void paginaTabla(String accion) {
        if (accion.equalsIgnoreCase("restarPagina")) {
            if (pagina > 0) {
                pagina--;
                cargaTabla();
            }
        } else if (accion.equalsIgnoreCase("sumarPagina")) {
            if ((pagina * LIMITE) + LIMITE < nRegistos) {
                pagina++;
                cargaTabla();
            }
        } else {
            System.out.println("Problemas en actividades de la tabla");
        }
        botonesPaginado();
    }
    
    private void botonesPaginado() {
        if (nRegistos <= LIMITE) {
            botonMas.setEnabled(false);
            botonMenos.setEnabled(false);
        } else if (nRegistos > LIMITE) {
            botonMas.setEnabled(true);
            botonMenos.setEnabled(true);
        }
        
        if ((pagina * LIMITE) + LIMITE >= nRegistos) {
            botonMas.setEnabled(false);
            botonMenos.setEnabled(true);
        }
        if (pagina == 0) botonMenos.setEnabled(false);
    }
}
