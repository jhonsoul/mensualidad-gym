/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos.general;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public class TextosIniciales {
    
    private String titulo;
    private String descripcion;
    private ImageIcon icono;
    
    public Object[] sesionPersonas() {
        titulo = "Sección personas";
        descripcion = "Esta sección se define los cliente y empleados del sistema.";
        icono = new ImageIcon(new ImageIcon(getClass().getResource("/geert-pieters-3RnkZpDqsEI-unsplash.jpg")).getImage().getScaledInstance(200, 170, Image.SCALE_DEFAULT));
        return new Object[]{titulo, descripcion, icono};
    }
    
    public Object[] sesionTarifas() {
        titulo = "Sección tarifas";
        descripcion = "Esta sección se define los costos de las tarifas.";
        icono = new ImageIcon(new ImageIcon(getClass().getResource("/bruno-kelzer-LvySG1hvuzI-unsplash.jpg")).getImage().getScaledInstance(200, 170, Image.SCALE_DEFAULT));
        return new Object[]{titulo, descripcion, icono};
    }
    
    public Object[] sesionVentas() {
        titulo = "Sección ventas";
        descripcion = "Esta sección se realizan los pago del tiempo que el usuario estara en el gimnacio.";
        icono = new ImageIcon(new ImageIcon(getClass().getResource("/clay-banks-E2HgkL3LaFE-unsplash.jpg")).getImage().getScaledInstance(200, 170, Image.SCALE_DEFAULT));
        return new Object[]{titulo, descripcion, icono};
    }
}
