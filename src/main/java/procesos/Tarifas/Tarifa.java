/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos.Tarifas;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Jhon
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tarifa {
    private String idTarifa;
    private String nombre;
    private int precio;
    private int Impuesto;
    private boolean eliminado;
}
