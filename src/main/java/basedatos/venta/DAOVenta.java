/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos.venta;

import basedatos.BaseDatos;
import javax.swing.table.DefaultTableModel;
import procesos.ventas.Venta;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public interface DAOVenta extends BaseDatos<Venta>{
    int cantidadRegistrosIndividual(String id) throws Exception;
    DefaultTableModel consultaIndividual(String id, int pagina, int limite) throws Exception;
}
