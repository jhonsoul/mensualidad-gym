/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos.venta;

import basedatos.ConexionExtras;
import static basedatos.ConexionExtras.LOGS;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.logging.Level;
import javax.swing.table.DefaultTableModel;
import procesos.ventas.Venta;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public class DAOImplVenta extends ConexionExtras implements DAOVenta{

    @Override
    public void registrar(Venta objetoGenerico) throws Exception {
        try {
            this.conectar();
            PreparedStatement pst;
            String querry = "INSERT INTO ventas (fkid_empleado, fkid_cliente, fkid_tarifa, fecha_termina, eliminado) VALUES (?, ?, ?, ?, ?);";
            pst = conexion.prepareStatement(querry);
            pst.setString(1, objetoGenerico.getIdentificacionEmpleado());
            pst.setString(2, objetoGenerico.getIdentificacionCliente());
            pst.setString(3, objetoGenerico.getIdentificacionTarifa());
            pst.setDate(4, Date.valueOf(objetoGenerico.getFechaTermino()));
            pst.setBoolean(5, objetoGenerico.isEliminado());
            pst.executeUpdate();
            pst.clearBatch();
            LOGS.info("La venta fue guardada exitosamente...");
            pst.close();
        } catch (Exception e) {
            LOGS.log(Level.INFO, "Se presento un error al momento de guardar la venta {0}", e.getLocalizedMessage());
        } finally {
            this.cerrarConexion();
        }
    }
    
    @Override
    public Object buscar(String id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(Venta objetoGenerico) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(Venta objetoGenerico) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DefaultTableModel lista(int pagina, int limite) throws Exception {
        this.conectar();
        
        int limitePagina = limite;
        int paginado = limitePagina * pagina;
        
        // Preparo el modelo para la tabla
        Object [][] datos = new Object[][]{};
        Object [] nombreColumnas = {"Nombres", "Tiempo", "Fecha de terminación"};
        DefaultTableModel modelo = new DefaultTableModel(datos, nombreColumnas);
        
        // Consulta a la base de datos limitada para hacer una paginación
        String query = "SELECT * FROM ventas JOIN personas ON ventas.fkid_cliente = personas.identificacion JOIN tarifas ON ventas.fkid_tarifa = tarifas.id_tarifa WHERE ventas.eliminado = false LIMIT " + limitePagina + " OFFSET " + paginado + ";";
        PreparedStatement pst = this.conexion.prepareStatement(query);
	ResultSet rs = pst.executeQuery();
        
        // Inicio objeto para cargar las filas
        Object[] fila;
        while (rs.next()) {
            String nombre = rs.getString("nombres") + " " +rs.getString("apellidos");
            String nombreTiempo = rs.getString("nombre");
            LocalDate fecha = rs.getDate("fecha_termina").toLocalDate();
            fila = new Object[]{nombre, nombreTiempo, fecha};
            modelo.addRow(fila);
        }
        this.cerrarConexion();
        return modelo;
    }

    @Override
    public int cantidadRegistros() throws Exception {
        String sql = "select count(*) as cantidad FROM ventas WHERE eliminado = false;";
        int cantidad = this.conteoRegistros(sql);
        return cantidad;
    }
    
    @Override
    public int cantidadRegistrosIndividual(String id) throws Exception {
        String sql = "select count(*) as cantidad FROM ventas WHERE fkid_cliente = ? AND ventas.eliminado = false;";
        int cantidad = this.conteoRegistros(sql, id);
        return cantidad;
    }

    @Override
    public DefaultTableModel consultaIndividual(String id, int pagina, int limite) throws Exception {
        this.conectar();
        
        int limitePagina = limite;
        int paginado = limitePagina * pagina;
        
        // Preparo el modelo para la tabla
        Object [][] datos = new Object[][]{};
        Object [] nombreColumnas = {"Fecha de termino", "Vendedor"};
        DefaultTableModel modelo = new DefaultTableModel(datos, nombreColumnas);
        
        // Consulta a la base de datos limitada para hacer una paginación
        String query = "SELECT * FROM ventas JOIN empleado ON ventas.fkid_empleado = empleado.fk_identificacion JOIN personas ON ventas.fkid_empleado = personas.identificacion WHERE ventas.fkid_cliente = ?  AND ventas.eliminado = false ORDER BY ventas.fecha_termina LIMIT " + limitePagina + " OFFSET " + paginado + ";";
        PreparedStatement pst = this.conexion.prepareStatement(query);
        pst.setString(1, id);
	ResultSet rs = pst.executeQuery();
        
        // Inicio objeto para cargar las filas
        Object[] fila;
        while (rs.next()) {
            LocalDate fechaTermina = rs.getDate("fecha_termina").toLocalDate();
            String nombreEmpleado = rs.getString("nombres");
            fila = new Object[]{fechaTermina, nombreEmpleado};
            modelo.addRow(fila);
        }
        this.cerrarConexion();
        return modelo;
    }

}
