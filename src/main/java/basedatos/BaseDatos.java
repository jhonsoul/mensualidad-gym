/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jhon
 * @param <T>
 */
public interface BaseDatos<T> {

    void registrar(T objetoGenerico) throws Exception;

    Object buscar(String id) throws Exception;
    
    void actualizar(T objetoGenerico) throws Exception;
    
    void eliminar(T objetoGenerico) throws Exception;
    
    DefaultTableModel lista(int pagina, int limite) throws Exception;
    
    int cantidadRegistros() throws Exception;

}
