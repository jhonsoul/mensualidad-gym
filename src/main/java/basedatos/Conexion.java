/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import lombok.Data;

/**
 * Clase dedicada a la conexión de la aplicaciòn a la base de datos.
 * @author Jhon
 */
@Data
public class Conexion {

    protected Connection conexion;

    private final String DB_IP = "192.168.1.4";
    private final String DB_PORT = "3306";
    private final String DB = "gym_db";
    private final String USER = "admin_gym";
    private final String PASSWORD = "p123456";
    private final String DB_URL = "jdbc:mariadb://" + DB_IP + ":" + DB_PORT + "/" + DB;
    private final String JDBC_DRIVER = "org.mariadb.jdbc.Driver";

    /**
     * Método para conectarse a una base de datos tipo maria DB
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    public void conectar() throws Exception {
        try {
            Class.forName(JDBC_DRIVER);
            conexion = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            throw e;
        }
    }

    /**
     * Cierra la conexión a la base de datos.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    public void cerrarConexion() throws Exception {
        if (conexion != null || !conexion.isClosed()) conexion.close();
    }
    
}
