/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos.personas;

import basedatos.ConexionExtras;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.swing.table.DefaultTableModel;
import procesos.personas.Cliente;

/**
 * Clase Que maneja las operaciones crud para los clientes, Usando el patro de diseño DAO para base de datos.
 * @author Jhon Alexander Buitrago
 */
public class DAOImplCliente extends ConexionExtras implements DAOCliente {
        
    /**
     * Método ultizado para registrar un nuevo cliente en el sistema.
     * @param cliente Objeto con la informacion necesaria para almacenar un cliente en la base de datos.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public void registrar(Cliente cliente) throws Exception {
        try {
            if (this.conexion == null || this.conexion.isClosed()) this.conectar();
            String querry = "SELECT * FROM cliente WHERE fk_identificacion = ?;";
            if (!this.existeInformacionPersona(querry, cliente.getIdentificaion())) {
                PreparedStatement pst;
                querry = "INSERT INTO cliente (fk_identificacion, correo, observaciones) VALUES (?, ?, ?);";
                pst = conexion.prepareStatement(querry);
                pst.setString(1, cliente.getIdentificaion());
                pst.setString(2, cliente.getCorreo());
                pst.setString(3, cliente.getObservaciones());
                var guardo = pst.executeUpdate();
                pst.clearBatch();
                if (guardo == 1) {
                    querry = "INSERT INTO personas (identificacion, nombres, apellidos, telefono, eliminado) VALUES (?, ?, ?, ?, ?);";
                    pst = this.conexion.prepareStatement(querry);
                    pst.setString(1, cliente.getIdentificaion());
                    pst.setString(2, cliente.getNombres());
                    pst.setString(3, cliente.getApellidos());
                    pst.setString(4, cliente.getTelefono());
                    pst.setBoolean(5, cliente.isEliminado());
                    pst.executeUpdate();
                    LOGS.info("El cliente fue guardado exitosamente...");
                }
                pst.close();
            } else {
                LOGS.info("La identificacion del cliente ya existe en la base de datos...");
            }
        } catch (SQLException e) {
            LOGS.log(Level.INFO, "Se presento un error al momento de guardar el cliente + {0}", e.getLocalizedMessage());
        } finally {
            this.cerrarConexion();
        }
    }
    
    /**
     * Método utilizado para buscar a los clientes.
     * @param id Parámetro que contiene la identificación del cliente para ser encontrada en la base de datos.
     * @return devuelve un objetos cliente para ser cargado en el sistema.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public Cliente buscar(String id) throws Exception {
        this.conectar();
        Cliente cliente = new Cliente();
        String querry = "SELECT * FROM cliente WHERE fk_identificacion = ?;";
        if (this.existeInformacionPersona(querry, id)) {
            querry = "SELECT * FROM personas INNER JOIN cliente ON cliente.fk_identificacion = personas.identificacion WHERE personas.identificacion = ?";
            PreparedStatement pst = this.conexion.prepareStatement(querry);
            pst.setString(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                cliente.setIdentificaion(rs.getString("identificacion"));
                cliente.setNombres(rs.getString("nombres"));
                cliente.setApellidos(rs.getString("apellidos"));
                cliente.setTelefono(rs.getString("telefono"));
                cliente.setCorreo(rs.getString("correo"));
                cliente.setObservaciones(rs.getString("observaciones"));
            }
            LOGS.info("Cliente cargado correctamente...");
        } else {
            LOGS.info("Cliente inexistente...");
            this.cerrarConexion();
            return null;
        }
        this.cerrarConexion();
        return cliente;
    }
    
    /**
     * Método utilizado para actualizar la informacion de un cliente.
     * @param cliente Contiene los valores necesarios para actualizar el cliente.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public void actualizar(Cliente cliente) throws Exception {
        try {
            this.conectar();
            String querry = "SELECT * FROM cliente WHERE fk_identificacion = ?;";
            if (this.existeInformacionPersona(querry, cliente.getIdentificaion())) {
                PreparedStatement pst;
                querry = "UPDATE cliente SET correo = ?, observaciones = ? WHERE fk_identificacion = ?;";
                pst = conexion.prepareStatement(querry);
                pst.setString(1, cliente.getCorreo());
                pst.setString(2, cliente.getObservaciones());
                pst.setString(3, cliente.getIdentificaion());
                var guardo = pst.executeUpdate();
                pst.clearBatch();
                if (guardo == 1) {
                    querry = "UPDATE personas SET nombres = ?, apellidos = ?, telefono = ? WHERE identificacion = ?;";
                    pst = this.conexion.prepareStatement(querry);
                    pst.setString(1, cliente.getNombres());
                    pst.setString(2, cliente.getApellidos());
                    pst.setString(3, cliente.getTelefono());
                    pst.setString(4, cliente.getIdentificaion());
                    pst.executeUpdate();
                    LOGS.info("Cliente actualizado correctamente...");
                    }
                pst.close();
            } else {
                LOGS.info("La identificacion del cliente no existe...");
            }
        } catch (SQLException e) {
            LOGS.info("Error en la interacción de la base de datos");
        } finally {
            this.cerrarConexion();
        }
    }

    /**
     * Método que elimina de forma logica a un cliente.
     * @param cliente Es un objeto cliente con la informacion necesarias para eliminar un cliente.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public void eliminar(Cliente cliente) throws Exception {
        this.conectar();
        String query = "SELECT * FROM cliente WHERE fk_identificacion = ?;";
        if (this.existeInformacionPersona(query, cliente.getIdentificaion())) {
            query = "UPDATE personas SET eliminado = ? WHERE identificacion = ?;";
            PreparedStatement pst = this.conexion.prepareStatement(query);
            pst.setBoolean(1, true);
            pst.setString(2, cliente.getIdentificaion());
            pst.executeUpdate();
            LOGS.info("Cliente eliminado correctamante...");
        } else {
            LOGS.info("El cliente expecificado no existe...");
        }
        this.cerrarConexion();
    }

    /**
     * Método utilizado para cargar la información de la base de datos a un modelo para luego ser cargado en una tabla.
     * @param pagina Parámetro que indica el numero de la pagina a consultar.
     * @param limite Contiene un valor numerico que indica el limite de registros que seran visualizados en la tabla.
     * @return Entrega un modelo de tabla listo para ser carjado en la parte de vista de la aplicación.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public DefaultTableModel lista(int pagina, int limite) throws Exception {
        this.conectar();
        
        int limitePagina = limite;
        int paginado = limitePagina * pagina;
        
        // Preparo el modelo para la tabla
        Object [][] datos = new Object[][]{};
        Object [] nombreColumnas = {"Identificación", "Nombres", "Apellidos", "Teléfono", "Correo", "Observaciones"};
        DefaultTableModel modelo = new DefaultTableModel(datos, nombreColumnas);
        
        // Consulta a la base de datos limitada para hacer una paginación
        String query = "SELECT * FROM personas JOIN cliente ON cliente.fk_identificacion = personas.identificacion WHERE eliminado = false LIMIT " + limitePagina + " OFFSET " + paginado + ";";
        PreparedStatement pst = this.conexion.prepareStatement(query);
	ResultSet rs = pst.executeQuery();
        
        // Inicio objeto para cargar las filas
        Object[] fila;
        while (rs.next()) {
            String identificacion = rs.getString("identificacion");
            String nombres = rs.getString("nombres");
            String apellidos = rs.getString("apellidos");
            String telefono = rs.getString("telefono");
            String correo = rs.getString("correo");
            String observaciones = rs.getString("observaciones");
            fila = new Object[]{identificacion, nombres, apellidos, telefono, correo, observaciones};
            modelo.addRow(fila);
        }
        this.cerrarConexion();
        return modelo;
    }

    /**
     * Método que entrega la cantidad de registros de clientes.
     * @return Entrega un valor numerico con la cantidad de clientes en la base de datos.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public int cantidadRegistros() throws Exception {
        String sql = "select count(*) as cantidad FROM personas JOIN cliente ON cliente.fk_identificacion = personas.identificacion WHERE eliminado = false;";
        int cantidad = this.conteoRegistros(sql);
        return cantidad;
    }

}
