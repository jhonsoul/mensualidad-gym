/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos.personas;

import basedatos.Conexion;
import basedatos.ConexionExtras;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import procesos.personas.Empleado;
import procesos.general.EncriptadorDesEncriptador;

/**
 * Clase Que maneja las operaciones crud para los empleados, Usando el patro de diseño DAO para base de datos.
 * @author Jhon
 */
public class DAOImplEmpleado extends ConexionExtras implements DAOEmpleado{

    /**
     * Método ultizado para registrar un nuevo empleado en el sistema.
     * @param empleado Objeto con la informacion necesaria para almacenar un empleado en la base de datos.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public void registrar(Empleado empleado) throws Exception {
        try {
            if (this.conexion == null || this.conexion.isClosed()) this.conectar();
            String query = "SELECT * FROM empleado WHERE fk_identificacion = ?;";
            if (!this.existeInformacionPersona(query, empleado.getIdentificaion())) {
                //Verifica si existe el mismo usuario.
                    if (!verificaUsuario(empleado.getUsuario())) {
                        if (this.conexion == null || this.conexion.isClosed()) this.conectar();
                        EncriptadorDesEncriptador od = new EncriptadorDesEncriptador();
                        PreparedStatement pst;
                        query = "INSERT INTO empleado (fk_identificacion, usuario, contrasena) VALUES (?, ?, ?);";
                        pst = this.conexion.prepareStatement(query);
                        pst.setString(1, empleado.getIdentificaion());
                        pst.setString(2, empleado.getUsuario());
                        pst.setString(3, od.encriptar(empleado.getContrasena()));
                        var guardo = pst.executeUpdate();
                        if (guardo == 1) {
                            query = "INSERT INTO personas (identificacion, nombres, apellidos, telefono, eliminado) VALUES (?, ?, ?, ?, ?);";
                            pst = this.conexion.prepareStatement(query);
                            pst.setString(1, empleado.getIdentificaion());
                            pst.setString(2, empleado.getNombres());
                            pst.setString(3, empleado.getApellidos());
                            pst.setString(4, empleado.getTelefono());
                            pst.setBoolean(5, empleado.isEliminado());
                            pst.executeUpdate();
                            LOGS.info("Empleado guardado exitosamente...");
                        }
                        pst.clearBatch();
                        pst.close();
                    } else {
                        LOGS.info("El usuario del empleado ya existe en la base de datos...");
                    }
                } else {
                    LOGS.info("La identificación del empleado ya existe en la base de datos...");
                }
        } catch (SQLException e) {
            LOGS.log(Level.INFO, "Se presento un error al momento de guardar el empleado {0}", e.getLocalizedMessage());
        } finally {
            this.cerrarConexion();
        }
    }

    
    
    /**
     * Método utilizado para buscar a los empleados.
     * @param id Parámetro que contiene la identificación del empleado para ser encontrada en la base de datos.
     * @return devuelve un objetos empleado para ser cargado en el sistema.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public Empleado buscar(String id) throws Exception {
        if (this.conexion == null || this.conexion.isClosed()) this.conectar();
        Empleado empleado = new Empleado();
        String querry = "SELECT * FROM empleado WHERE fk_identificacion = ?;";
        if (this.existeInformacionPersona(querry, id)) {
            querry = "SELECT * FROM personas INNER JOIN empleado ON empleado.fk_identificacion = personas.identificacion WHERE personas.identificacion = ?";
            PreparedStatement pst = this.conexion.prepareStatement(querry);
            pst.setString(1, id);
            ResultSet rs = pst.executeQuery();
            EncriptadorDesEncriptador od = new EncriptadorDesEncriptador();
            while (rs.next()) {
                empleado.setIdentificaion(rs.getString("fk_identificacion"));
                empleado.setNombres(rs.getString("nombres"));
                empleado.setApellidos(rs.getString("apellidos"));
                empleado.setTelefono(rs.getString("telefono"));
                empleado.setUsuario(rs.getString("usuario"));
                empleado.setContrasena(od.desencriptar(rs.getString("contrasena")));
            }
            pst.clearBatch();
            pst.close();
            LOGS.info("Empleado cargado correctamente...");
        } else {
            LOGS.info("Empleado inexistente...");
            this.cerrarConexion();
            return null;
        }
        this.cerrarConexion();
        return empleado;
    }
    
    /**
     * Método utilizado para actualizar la informacion de un empleado.
     * @param empleado Contiene los valores necesarios para actualizar el cliente.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public void actualizar(Empleado empleado) throws Exception {
        try {
            if (this.conexion == null || this.conexion.isClosed()) this.conectar();
            String querry = "SELECT * FROM empleado WHERE fk_identificacion = ?;";
            EncriptadorDesEncriptador od = new EncriptadorDesEncriptador();
            if (this.existeInformacionPersona(querry, empleado.getIdentificaion())) {
                PreparedStatement pst;
                querry = "UPDATE empleado SET usuario = ?, contrasena = ? WHERE fk_identificacion = ?;";
                pst = conexion.prepareStatement(querry);
                pst.setString(1, empleado.getUsuario());
                pst.setString(2, od.encriptar(empleado.getContrasena()));
                pst.setString(3, empleado.getIdentificaion());
                var guardo = pst.executeUpdate();
                pst.clearBatch();
                if (guardo == 1) {
                    querry = "UPDATE personas SET nombres = ?, apellidos = ?, telefono = ? WHERE identificacion = ?;";
                    pst = this.conexion.prepareStatement(querry);
                    pst.setString(1, empleado.getNombres());
                    pst.setString(2, empleado.getApellidos());
                    pst.setString(3, empleado.getTelefono());
                    pst.setString(4, empleado.getIdentificaion());
                    pst.executeUpdate();
                    LOGS.info("Empleado actualizado correctamente...");
                }
                pst.clearBatch();
                pst.close();
            } else {
                LOGS.info("La identificacion del empleado no existe...");
            }
        } catch (SQLException e) {
            LOGS.info("Error en la interacción de la base de datos");
        } finally {
            this.cerrarConexion();
        }
    }

    /**
     * Método que elimina de forma logica a un cliente.
     * @param empleado Es un objeto empleado con la informacion necesarias para eliminar un cliente.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public void eliminar(Empleado empleado) throws Exception {
        if (this.conexion == null || this.conexion.isClosed()) this.conectar();
        String query = "SELECT * FROM empleado WHERE fk_identificacion = ?;";
        if (this.existeInformacionPersona(query, empleado.getIdentificaion())) {
            query = "UPDATE personas SET eliminado = ? WHERE identificacion = ?;";
            PreparedStatement pst = this.conexion.prepareStatement(query);
            pst.setBoolean(1, true);
            pst.setString(2, empleado.getIdentificaion());
            pst.executeUpdate();
            pst.clearBatch();
            pst.close();
            LOGS.info("Empleado eliminado correctamante...");
        } else {
            LOGS.info("El empleado expecificado no existe...");
        }
        this.cerrarConexion();
    }

    /**
     * Método utilizado para cargar la información de la base de datos a un modelo para luego ser cargado en una tabla.
     * @param pagina Parámetro que indica el numero de la pagina a consultar.
     * @param limite Contiene un valor numerico que indica el limite de registros que seran visualizados en la tabla.
     * @return Entrega un modelo de tabla listo para ser carjado en la parte de vista de la aplicación.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos. 
     */
    @Override
    public DefaultTableModel lista(int pagina, int limite) throws Exception {
        if (this.conexion == null || this.conexion.isClosed()) this.conectar();
        
        int limitePagina = limite;
        int paginado = limitePagina * pagina;
        
        // Preparo el modelo para la tabla
        Object [][] datos = new Object[][]{};
        Object [] nombreColumnas = {"Identificación", "Nombres", "Apellidos", "Teléfono", "Usuario"};
        DefaultTableModel modelo = new DefaultTableModel(datos, nombreColumnas);
        
        // Consulta a la base de datos limitada para hacer una paginación
        String query = "SELECT * FROM personas JOIN empleado ON empleado.fk_identificacion = personas.identificacion WHERE eliminado = false LIMIT " + limitePagina + " OFFSET " + paginado + ";";
        PreparedStatement pst = this.conexion.prepareStatement(query);
	ResultSet rs = pst.executeQuery();
        
        // Inicio objeto para cargar las filas
        Object[] fila;
        while (rs.next()) {
                String identificacion = rs.getString("identificacion");
                String nombres = rs.getString("nombres");
                String apellidos = rs.getString("apellidos");
                String telefono = rs.getString("telefono");
                String usuario = rs.getString("usuario");
                fila = new Object[]{identificacion, nombres, apellidos, telefono, usuario};
                modelo.addRow(fila);
        }
        pst.clearBatch();
        pst.close();
        this.cerrarConexion();
        return modelo;
    }
    
    /**
     * Método que entrega la cantidad de registros de empleados.
     * @return Entrega un valor numerico con la cantidad de empleado en la base de datos.
     * @throws Exception Lanza un error en el sistema si falla la interacción con la base de datos.
     */
    @Override
    public int cantidadRegistros() throws Exception {
        String sql = "SELECT COUNT(*) as cantidad FROM personas JOIN empleado ON empleado.fk_identificacion = personas.identificacion WHERE eliminado = false;";
        int cantidad = this.conteoRegistros(sql);
        return cantidad;
    }

    @Override
    public boolean verificaUsuario(String usuario) throws Exception {
        String query = "SELECT * FROM empleado WHERE usuario = ?;";
        try {
            if (this.conexion == null || this.conexion.isClosed()) this.conectar();
            ResultSet rs;
            try (PreparedStatement pst = this.conexion.prepareStatement(query)) {
                pst.setString(1, usuario);
                rs = pst.executeQuery();
                pst.clearBatch();
                pst.close();
            }
            boolean existeUsuario = rs.next();
            this.cerrarConexion();
            return existeUsuario;
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public Empleado verificaInicioSesion(String usuario, String contrasena) throws Exception {
        String query = "SELECT * FROM personas JOIN empleado ON empleado.fk_identificacion = personas.identificacion WHERE usuario = ? AND contrasena = ? AND eliminado = false;";
        Empleado empleado = new Empleado();
        try {
            if (this.conexion == null || this.conexion.isClosed()) this.conectar();
            PreparedStatement pst = this.conexion.prepareStatement(query);
            EncriptadorDesEncriptador od = new EncriptadorDesEncriptador();
            pst.setString(1, usuario);
            pst.setString(2, od.encriptar(contrasena));
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                empleado.setIdentificaion(rs.getString("fk_identificacion"));
                empleado.setNombres(rs.getString("nombres"));
                empleado.setApellidos(rs.getString("apellidos"));
                empleado.setTelefono(rs.getString("telefono"));
                empleado.setUsuario(rs.getString("usuario"));
                empleado.setContrasena(od.desencriptar(rs.getString("contrasena")));
            }
            this.cerrarConexion();
            return empleado;
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        return empleado;
    }
    
}
