/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos.personas;

import procesos.personas.Empleado;
import basedatos.BaseDatos;

/**
 *
 * @author Jhon
 */
public interface DAOEmpleado extends BaseDatos<Empleado>{
    boolean verificaUsuario(String id) throws Exception;
    Empleado verificaInicioSesion(String usuario, String contrasena) throws Exception;
}
