/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos.Tarifas;

import basedatos.BaseDatos;
import java.util.ArrayList;
import procesos.Tarifas.Tarifa;

/**
 *
 * @author Jhon
 */
public interface DAOTarifa extends BaseDatos<Tarifa>{
    public ArrayList<String> cargaIdTarifas()throws Exception;
}
