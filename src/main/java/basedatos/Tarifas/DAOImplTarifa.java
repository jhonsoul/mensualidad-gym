/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos.Tarifas;

import basedatos.ConexionExtras;
import static basedatos.ConexionExtras.LOGS;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.swing.table.DefaultTableModel;
import procesos.Tarifas.Tarifa;

/**
 *
 * @author Jhon
 */
public class DAOImplTarifa extends ConexionExtras implements DAOTarifa {

    @Override
    public void registrar(Tarifa objetoGenerico) throws Exception {
        try {
            this.conectar();
            String querry = "SELECT * FROM tarifas WHERE id_tarifa = ?;";
            if (!this.existeInformacionPersona(querry, objetoGenerico.getIdTarifa())) {
                PreparedStatement pst;
                querry = "INSERT INTO tarifas (id_tarifa, nombre, precio, impuesto, eliminado) VALUES (?, ?, ?, ?, ?);";
                pst = conexion.prepareStatement(querry);
                pst.setString(1, objetoGenerico.getIdTarifa());
                pst.setString(2, objetoGenerico.getNombre());
                pst.setInt(3, objetoGenerico.getPrecio());
                pst.setInt(4, objetoGenerico.getImpuesto());
                pst.setBoolean(5, objetoGenerico.isEliminado());
                pst.executeUpdate();
                pst.clearBatch();
                LOGS.info("La tarifa fue guardado exitosamente...");
                pst.close();
            } else {
                LOGS.info("La identificacion de la tarifa ya existe en la base de datos...");
            }
        } catch (Exception e) {
            LOGS.log(Level.INFO, "Se presento un error al momento de guardar la tarifa + {0}", e.getLocalizedMessage());
        } finally {
            this.cerrarConexion();
        }
    }

    @Override
    public Tarifa buscar(String id) throws Exception {
        this.conectar();
        Tarifa tarifa = new Tarifa();
        String querry = "SELECT * FROM tarifas WHERE id_tarifa = ?;";
        if (this.existeInformacionPersona(querry, id)) {
            PreparedStatement pst = this.conexion.prepareStatement(querry);
            pst.setString(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                tarifa.setIdTarifa(rs.getString("id_tarifa"));
                tarifa.setNombre(rs.getString("nombre"));
                tarifa.setPrecio(rs.getInt("precio"));
                tarifa.setImpuesto(rs.getInt("impuesto"));
                tarifa.setEliminado(rs.getBoolean("eliminado"));
            }
            LOGS.info("La tarifa fue cargada correctamente...");
        } else {
            LOGS.info("Tarifa inexistente...");
            this.cerrarConexion();
            return null;
        }
        this.cerrarConexion();
        return tarifa;
    }

    @Override
    public void actualizar(Tarifa objetoGenerico) throws Exception {
        try {
            this.conectar();
            String querry = "SELECT * FROM tarifas WHERE id_tarifa = ?;";
            if (this.existeInformacionPersona(querry, objetoGenerico.getIdTarifa())) {
                PreparedStatement pst;
                querry = "UPDATE tarifas SET nombre = ?, precio = ?, impuesto = ? WHERE id_tarifa = ?;";
                pst = conexion.prepareStatement(querry);
                pst.setString(1, objetoGenerico.getNombre());
                pst.setInt(2, objetoGenerico.getPrecio());
                pst.setInt(3, objetoGenerico.getImpuesto());
                pst.setString(4, objetoGenerico.getIdTarifa());
                pst.executeUpdate();
                pst.close();
            } else {
                LOGS.info("La identificacion de la tarifa no existe...");
            }
        } catch (Exception e) {
            LOGS.info("Error en la interacción de la base de datos");
        } finally {
            this.cerrarConexion();
        }
    }

    @Override
    public void eliminar(Tarifa objetoGenerico) throws Exception {
        this.conectar();
        String query = "SELECT * FROM tarifas WHERE id_tarifa = ?;";
        if (this.existeInformacionPersona(query, objetoGenerico.getIdTarifa())) {
            query = "UPDATE tarifas SET eliminado = ? WHERE id_tarifa = ?;";
            PreparedStatement pst = this.conexion.prepareStatement(query);
            pst.setBoolean(1, true);
            pst.setString(2, objetoGenerico.getIdTarifa());
            pst.executeUpdate();
            LOGS.info("La tarifa fue eliminado correctamante...");
        } else {
            LOGS.info("La tarifa expecificado no existe...");
        }
        this.cerrarConexion();
    }

    @Override
    public DefaultTableModel lista(int pagina, int limite) throws Exception {
        this.conectar();
        
        int limitePagina = limite;
        int paginado = limitePagina * pagina;
        
        // Preparo el modelo para la tabla
        Object [][] datos = new Object[][]{};
        Object [] nombreColumnas = {"Identificación", "Nombre", "Precio", "Impuesto"};
        DefaultTableModel modelo = new DefaultTableModel(datos, nombreColumnas);
        
        // Consulta a la base de datos limitada para hacer una paginación
        String query = "SELECT * FROM tarifas WHERE eliminado = false LIMIT " + limitePagina + " OFFSET " + paginado + ";";
        PreparedStatement pst = this.conexion.prepareStatement(query);
	ResultSet rs = pst.executeQuery();
        
        // Inicio objeto para cargar las filas
        Object[] fila;
        while (rs.next()) {
            String identificacion = rs.getString("id_tarifa");
            String nombre = rs.getString("nombre");
            int precio = rs.getInt("precio");
            int impuesto = rs.getInt("impuesto");
            fila = new Object[]{identificacion, nombre, precio, impuesto};
            modelo.addRow(fila);
        }
        this.cerrarConexion();
        return modelo;
    }

    @Override
    public int cantidadRegistros() throws Exception {
        String sql = "select count(*) as cantidad FROM tarifas WHERE eliminado = false;";
        int cantidad = this.conteoRegistros(sql);
        return cantidad;
    }

    @Override
    public ArrayList<String> cargaIdTarifas() throws Exception {
        this.conectar();
        ArrayList<String> lista = new ArrayList<>();
        String query = "SELECT id_tarifa FROM tarifas WHERE eliminado = false;";
        PreparedStatement pst = this.conexion.prepareStatement(query);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            lista.add(rs.getString("id_tarifa"));
        }
        this.cerrarConexion();
        return lista;
    }
    
}
