/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Esta clase se usa para agregar métodos diferentes a la conexión y 
 * desconectar de la base de datos, estos métodos se utilizan frecuentemente 
 * para averiguar información de la base de datos que suele ser común para 
 * las demás clases controladoras.
 * @author Jhon Alexander Buitrago
 */
public class ConexionExtras extends Conexion{
    
    // Objeto para enviar mensajes de consola.
    public static final Logger LOGS = Logger.getLogger("Base de datos");
    
    /**
     * Método usado para averiguar si existe un registro en la base de datos.
     * @param query. Parámetro que se entrega al método con la sentencia sql para 
     * ser consultada en la base de datos.
     * @param id. Parámetro que contiene la identificación para ser 
     * encontrada en la base de datos.
     * @return Devuelve verdadero si existe en la base de datos.
     * @throws Exception Muestra un error si falla la busqueda en la base de datos.
     */
    public boolean existeInformacionPersona(String query, String id) throws Exception {
        try {
            PreparedStatement pst = conexion.prepareStatement(query);
            pst.setString(1, id);
            ResultSet rs = pst.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    
    /**
     * Método solo para contar la cantidad de registro de una tabla.
     * @param query Parámetro que se entrega al método la sentencia sql para 
     * ser consultada en la base de datos.
     * @return Regresa la cantidad de registros en la base de datos.
     * @throws Exception Muestra un error si falla la consulta en la base de datos.
     */
    public int conteoRegistros(String query) throws Exception {
        if (this.conexion == null || this.conexion.isClosed()) conectar();
        int cantidad = 0;
        PreparedStatement pst = conexion.prepareStatement(query);
        try (ResultSet rs = pst.executeQuery()) {
            if (rs.next()) {
                cantidad = rs.getInt("cantidad");
            }
        }
        cerrarConexion();
        return cantidad;
    }
    
    public int conteoRegistros(String query, String id) throws Exception {
        if (this.conexion == null || this.conexion.isClosed()) conectar();
        int cantidad = 0;
        PreparedStatement pst = conexion.prepareStatement(query);
        pst.setString(1, id);
        try (ResultSet rs = pst.executeQuery()) {
            if (rs.next()) {
                cantidad = rs.getInt("cantidad");
            }
        }
        cerrarConexion();
        return cantidad;
    }
}
