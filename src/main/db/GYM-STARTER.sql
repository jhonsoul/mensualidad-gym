CREATE OR REPLACE DATABASE  gym_db;
--Despues de la ip se agrega la ip de donde se trabajara y p123456 sera la contraseña.
GRANT ALL ON gym_db.* TO 'admin_gym'@'192.168.1.2' IDENTIFIED BY 'p123456' WITH GRANT OPTION;
create table if not exists gym_db.personas (id_personas int unsigned not null auto_increment comment 'Llave primaria', identificacion varchar(10) not null, nombres varchar(30) not null, apellidos varchar(30), telefono varchar(10), eliminado bit not null, primary key(id_personas));
create table if not exists gym_db.empleado (id_empleado int unsigned not null auto_increment comment 'Llave primaria', fk_identificacion varchar(10) not null, usuario varchar(10) not null, contrasena varchar(30) not null, primary key(id_empleado));
create table if not exists gym_db.cliente (id_cliente int unsigned not null auto_increment comment 'Llave primaria', fk_identificacion varchar(10) not null, correo varchar(30), observaciones varchar(300), primary key(id_cliente));
create table if not exists gym_db.tarifas (id int unsigned not null auto_increment comment 'Llave primaria', id_tarifa varchar(30) not null, nombre varchar(30) not null, precio int not null, impuesto int not null, eliminado bit not null, primary key(id));
create table if not exists gym_db.ventas (id_venta int unsigned not null auto_increment comment 'Llave primaria', fkid_empleado varchar(10) not null, fkid_cliente varchar(10) not null, fkid_tarifa varchar(30) not null, fecha_termina date not null,eliminado bit not null, primary key(id_venta));
